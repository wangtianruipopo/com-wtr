package com.wtr.spring.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringCloudApplication
@EnableEurekaClient
public class SpringGatewayApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringGatewayApplication.class, args);
	}
}
