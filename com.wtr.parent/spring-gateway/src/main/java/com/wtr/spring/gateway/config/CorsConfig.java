package com.wtr.spring.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.util.pattern.PathPatternParser;

@Configuration
public class CorsConfig {
	@Bean
	public CorsWebFilter corsFilter() {
		// 1.添加CORS配置信息
		CorsConfiguration config = new CorsConfiguration();
		config.addAllowedMethod("*");
		// 设置允许的网站域名，如果全部允许则设置为*
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		// 2.添加映射路径
		UrlBasedCorsConfigurationSource configSource = new UrlBasedCorsConfigurationSource(new PathPatternParser());
		configSource.registerCorsConfiguration("/**", config);
		// 3.返回新的Filter.
		return new CorsWebFilter(configSource);
	}
}
