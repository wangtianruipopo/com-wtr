package com.wtr.auth.filter;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.google.protobuf.ServiceException;
import com.wtr.auth.po.JwtUser;
import com.wtr.auth.util.JwtUtil;

@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

	@Value("${jwt.header}")
    private String tokenHeader;
	
	@Autowired
    private JwtUtil jwtTokenUtil;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String authToken = request.getHeader(this.tokenHeader);
		// 获取用户信息
		JwtUser userDetails = jwtTokenUtil.getUserFromToken(authToken);
		if (userDetails != null) {
			String username = userDetails.getUsername();
			if (username != null && jwtTokenUtil.containToken(username, authToken) && SecurityContextHolder.getContext().getAuthentication() == null) {
				if (jwtTokenUtil.validateToken(authToken)) {
					Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
					UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, authorities);
					authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					System.out.println("authenticated user " + username + ", setting security context");
					SecurityContextHolder.getContext().setAuthentication(authentication);
				} else {
					throw new ServletException("token失效,请重新登录");
				}
			}
		}
		filterChain.doFilter(request, response);
	}

}
