package com.wtr.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.wtr.auth.mapper.UserMapper;
import com.wtr.auth.po.JwtUser;
import com.wtr.auth.po.User;

@Service
public class UserService implements UserDetailsService {
	
	@Autowired
	private UserMapper mapper;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = mapper.selectByUsername(username);
		JwtUser _user = new JwtUser(user);
		// 1、根据请求用户名向数据库中查询用户信息
		if (user == null) {
			throw new UsernameNotFoundException("用户名或密码错误"); 
		}
		// 2、查询用户权限
		return _user;
	}

}
