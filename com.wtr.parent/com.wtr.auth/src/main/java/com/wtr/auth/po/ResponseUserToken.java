package com.wtr.auth.po;

import lombok.Data;

@Data
public class ResponseUserToken {
	private String token;
	private JwtUser user;
	
	public ResponseUserToken(String token, JwtUser userDetail) {
		this.token = token;
		this.user = userDetail;
		userDetail.setPassword(null);
	}
}
