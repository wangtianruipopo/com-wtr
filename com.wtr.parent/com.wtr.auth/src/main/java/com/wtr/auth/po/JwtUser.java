package com.wtr.auth.po;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Getter;

public class JwtUser extends User implements UserDetails {
	private static final long serialVersionUID = -1841832825603313290L;

	public JwtUser() {}
	
	/**
	 * 权限
	 */
	@Getter
	private Collection<? extends GrantedAuthority> authorities;

	public JwtUser(String userId, String username, String account, List<String> authorities) {
		this.id = userId;
		this.nickname = username;
		this.username = account;
		Set<SimpleGrantedAuthority> simpleGrantedAuthorities = new HashSet<>();
		if (CollectionUtils.isNotEmpty(authorities)) {
			for (String code : authorities) {
				SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(code);
				simpleGrantedAuthorities.add(simpleGrantedAuthority);
			}
		}
		this.authorities = simpleGrantedAuthorities;
	}

	public JwtUser(User user) {
		this.id = user.getId();
        this.nickname = user.getNickname();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.accountNonExpired = user.isAccountNonExpired();
        this.accountNonLocked = user.isAccountNonLocked();
        this.credentialsNonExpired = user.isCredentialsNonExpired();
        Set<SimpleGrantedAuthority> simpleGrantedAuthorities = new HashSet<>();
        this.authorities = simpleGrantedAuthorities;
	}

}
