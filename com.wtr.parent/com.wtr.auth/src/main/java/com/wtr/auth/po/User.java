package com.wtr.auth.po;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wtr.common.base.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("user")
public class User extends BaseEntity {
	private static final long serialVersionUID = 8890348550620172060L;
	protected String username;
	protected String password;
	protected String nickname;
	protected Date createDate;
	protected Date lastLoginTime;
	protected String lastLoginAdress;
	protected boolean accountNonExpired = true;
	protected boolean accountNonLocked = true;
	protected boolean credentialsNonExpired = true;
	protected boolean enabled = true;
}
