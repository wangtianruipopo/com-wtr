package com.wtr.auth.util;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.wtr.auth.po.JwtUser;
import com.wtr.auth.po.ResponseUserToken;
import com.wtr.common.vo.WrappedResult;

/**
 * 登录成功
 * @author wangtianrui
 *
 */
@Component
public class AjaxAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Autowired
	private JwtUtil jwt;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		WrappedResult<ResponseUserToken> result = WrappedResult.success(
			WrappedResult.SUCCESS_CODE, "登录成功",
			new ResponseUserToken(jwt.generateAccessToken((JwtUser) authentication.getPrincipal()), (JwtUser) authentication.getPrincipal())
		);
		response.getWriter().write(JSON.toJSONString(result, SerializerFeature.BrowserCompatible));
	}

}
