package com.wtr.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import com.wtr.auth.filter.JwtAuthenticationTokenFilter;
import com.wtr.auth.util.AjaxAuthenticationEntryPoint;

@Component
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;
	@Autowired
	private UserDetailsService userDetailsService;
	@Value("${jwt.expiration}")
	private int validate;
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	/**
	 * 未登录
	 */
	@Autowired
	private AuthenticationEntryPoint ajaxAuthenticationEntryPoint;
	
	/**
	 * 登录成功
	 */
	@Autowired
	private AuthenticationSuccessHandler authenticationSuccessHandler;
	
	/**
	 * 登录失败
	 */
	@Autowired
	private AuthenticationFailureHandler authenticationFailureHandler;
	
	/**
	 * 无权访问
	 */
	@Autowired
	private AccessDeniedHandler accessDeniedHandler;
	
	/**
	 * 注销
	 */
	@Autowired
	private LogoutSuccessHandler logoutSuccessHandler;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// 添加自定义的userDetailsService认证
		auth.userDetailsService(this.userDetailsService);
	}
	
	@Bean(name = BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
//		http.cors()
//		.and()
//		.csrf().disable()
//		.httpBasic().authenticationEntryPoint(ajaxAuthenticationEntryPoint)
//		.and().authorizeRequests()
//		.anyRequest()
//		// 其他 url 需要身份认证
//		.authenticated()
//		.and()
//		// 开启登录
//		.formLogin()
//		.successHandler(authenticationSuccessHandler)
//		.failureHandler(authenticationFailureHandler)
//		.and()
//		.logout()
//		.logoutSuccessHandler(logoutSuccessHandler)
//		.permitAll();
//	// 无权访问 JSON 格式的数据
//	http.exceptionHandling().accessDeniedHandler(accessDeniedHandler);
		
		
		http.csrf().disable()
			.cors().and()
			// 使用 JWT，关闭token
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.httpBasic()
			// 未经过认证的用户访问受保护的资源
			.authenticationEntryPoint(ajaxAuthenticationEntryPoint)
			.and()
			.authorizeRequests()
			// 任何用户都可以访问URL以"/resources/", equals "/signup", 或者 "/about"开头的URL
			// 以 "/admin/" 开头的URL只能由拥有 "ROLE_ADMIN"角色的用户访问。请注意我们使用 hasRole 方法
//			.antMatchers("/admin").hasRole("ADMIN")
//			.antMatchers("/user").access("hasRole('USER') or hasRole('ADMIN') ")
//			.antMatchers("/employee").access("hasRole('EMPLOYEE') or hasRole('ADMIN') ")
			.anyRequest().authenticated()
			.and()
			.formLogin()
			.loginProcessingUrl("/login")
			.successHandler(authenticationSuccessHandler)
			.failureHandler(authenticationFailureHandler)
			.and()
			.logout()
			.logoutSuccessHandler(logoutSuccessHandler)
			.permitAll();
		// 无权访问 JSON 格式的数据
//		http.exceptionHandling().accessDeniedHandler(accessDeniedHandler);
		http.rememberMe().rememberMeParameter("remember-me")
        	.userDetailsService(userDetailsService).tokenValiditySeconds(validate);
		http.exceptionHandling()
        	.and().addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
	}
	
//	@Autowired
//	private UserDetailsService customUserDetailsService;
//	
//	/**
//	 * 未登录
//	 */
//	@Autowired
//	private AuthenticationEntryPoint ajaxAuthenticationEntryPoint;
//	
//	/**
//	 * 登录成功
//	 */
//	@Autowired
//	private AuthenticationSuccessHandler authenticationSuccessHandler;
//	
//	/**
//	 * 登录失败
//	 */
//	@Autowired
//	private AuthenticationFailureHandler authenticationFailureHandler;
//	
//	/**
//	 * 无权访问
//	 */
//	@Autowired
//	private AccessDeniedHandler accessDeniedHandler;
//	
//	/**
//	 * 注销
//	 */
//	@Autowired
//	private LogoutSuccessHandler logoutSuccessHandler;
//	
//	/**
//	 * 认证管理器
//	 * 1、认证信息提供方式(用户名、密码、当前用户的资源权限)
//	 * 2、可采用内存存储方式，也可能采用数据库方式
//	 * @param auth
//	 * @throws Exception
//	 */
//	@Override
//	public void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.userDetailsService(customUserDetailsService);
//	}
//	/**
//	 * 资源权限配置
//	 */
//	@Override
//	public void configure(HttpSecurity http) throws Exception {
//		http.cors()
//			.and()
//			.csrf().disable()
//			.httpBasic().authenticationEntryPoint(ajaxAuthenticationEntryPoint)
//			.and().authorizeRequests()
//			.anyRequest()
//			// 其他 url 需要身份认证
//			.authenticated()
//			.and()
//			// 开启登录
//			.formLogin()
//			.successHandler(authenticationSuccessHandler)
//			.failureHandler(authenticationFailureHandler)
//			.and()
//			.logout()
//			.logoutSuccessHandler(logoutSuccessHandler)
//			.permitAll();
//		// 无权访问 JSON 格式的数据
//		http.exceptionHandling().accessDeniedHandler(accessDeniedHandler);
//		http
//	        // CRSF禁用，因为不使用session
//	        .csrf().disable()
//	        // 认证失败处理类
//	        .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
//	        // 基于token，所以不需要session
//	        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
//	        // 过滤请求
//	        .authorizeRequests()
//	        // 对于登录login 验证码captchaImage 允许匿名访问
//	        .antMatchers("/login","/captchaImage").anonymous()
//	        .antMatchers(
//	                HttpMethod.GET,
//	                "/*.html",
//	                "/**/*.html",
//	                "/**/*.css",
//	                "/**/*.js"
//	        ).permitAll()
//	        .antMatchers("/profile/**").permitAll()
//	        .antMatchers("/common/download**").anonymous()
//	        .antMatchers("/common/download/resource**").anonymous()
//	        .antMatchers("/swagger-ui.html").anonymous()
//	        .antMatchers("/swagger-resources/**").anonymous()
//	        .antMatchers("/webjars/**").anonymous()
//	        .antMatchers("/*/api-docs").anonymous()
//	        .antMatchers("/druid/**").anonymous()
//	        //deacy自定义放行的url--数据字典
//	        .antMatchers("/system/dict/data/dictType/**").permitAll()
//	        //deacy自定义放行的url--lego模块所有
//	        .antMatchers("/lego/**").permitAll()
//	        .antMatchers("/capacity/regist",
//	                "/capacity/login",
//	                "/capacity/resetPwd",
//	                "/capacity/accountLevelUp"
//	        ).permitAll()
//	        .antMatchers("/myTest/**").permitAll()
//	        // 除上面外的所有请求全部需要鉴权认证
//	        .anyRequest().authenticated()
//	        .and()
//	        .headers().frameOptions().disable();
//		http.logout().logoutUrl("/logout").logoutSuccessHandler(logoutSuccessHandler);
//		// 添加JWT filter---这个就是添加的token拦截器
//		http.addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
//	}
	
}
